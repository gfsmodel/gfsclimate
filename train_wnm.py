from datetime import datetime, timedelta
from enum import Enum
from os.path import join
from utils.model.wnm import Wavenet
from utils.preprocessing.preprocess_train_wnm import window_slide, normalization, var_data
from configparser import ConfigParser
import numpy as np
import logging
import requests
import os
import tensorflow as tf
import pytz
import pygrib
from pytz import timezone, utc
import argparse


c = ConfigParser()
c.read('remote.conf')

parser = argparse.ArgumentParser(description='running some cron jobs')
parser.add_argument('--year', '-y', type=int)
parser.add_argument('--month', '-m', type=int)
parser.add_argument('--day','-d', type=int)
parser.add_argument('--hour', '-hr', type=int)
parser.add_argument('--start', '-str', type=int)
parser.add_argument('--stop', '-stp', type=int)
parser.add_argument('--name', type=str)
args = parser.parse_args()

def datapath_file (year,month,day,hour, base):
    t1 = datetime(year,month,day,hour)
    # make it 2 month
    # 31 + 8*4 = 63
    t_250 = t1 - timedelta(days=63,hours=6)
    t128 = t1 + timedelta(days=16)
    count = t1 - timedelta(days=63,hours=6)
    datapath = []
    timee = ['00','06','12','18']
    infile = ['003','006']
    while count < t128:
        iday = str(count)[8:10]
        imonth = str(count)[5:7]
        if str(count)[11:13] in timee:
            datapath.append(join(base,"2020{}/gfs_4_2020{}{}_{}00_{}.grb2".format(imonth,imonth,iday,str(count)[11:13].zfill(2),infile[0])))
        else:
            datapath.append(join(base, "2020{}/gfs_4_2020{}{}_{}00_{}.grb2".format(imonth,imonth,iday,str(int(str(count)[11:13])-3).zfill(2),infile[1])))
        count += timedelta(hours=3)
    return datapath

def get_time():
    delay_days = 4
    download_day = datetime.today() - timedelta(days=delay_days)
    hour = 0
    return download_day.year, download_day.month, download_day.day, hour

def hours_minutes_seconds(td):
    return td.seconds//3600, (td.seconds//60)%60, td.seconds%60

if __name__ == "__main__":
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        for gpu in gpus:
            try:
                tf.config.experimental.set_memory_growth(gpu, True)
            except RuntimeError as e:
                print(e)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")

    config = c['wavenet']
    base = config['base']
    csv_path=config['csv']
    model_dir=config['model_dir']
    epoch = 10
    
    year, month, day, hour = args.year, args.month, args.day, args.hour
    # year, month, day, hour = 2020, 10, 1, 0

    # train
    # logger.debug("Start Training")
    # time the training
    start_time = datetime.today()
    var_nums = [61, 71, 81, 91, 101, 111, 121, 131, 141, 151, 161, 171, 181, 191, 201, 211, 221]
    for i in var_nums:
        start_train_var = datetime.today()

        data_paths = datapath_file(year,month,day,hour, base)
        data = var_data(data_paths,i, csv_path)
        xtrain, ytrain = window_slide(data, step=8*8, input_size=250, predict_size=128)

        xtrain = xtrain.transpose((0,2,1))
        ytrain = ytrain.transpose((0,2,1))

        xtrain = np.concatenate(xtrain)
        ytrain = np.concatenate(ytrain)
        print(xtrain.shape)
        print(ytrain.shape)

        xdata_norm,ydata_norm,maxval,minval = normalization(xtrain, ytrain)
        model = Wavenet(c['wavenet'], i)
        model.fit(
            xtrain=xdata_norm,
            ytrain=ydata_norm,
            epochs=10,
            batch_size=128)

        # logger.debug(f"Successfully trained on var {str(i).zfill(3)}")
        print(f"Successfully trained on var {str(i).zfill(3)}")
        end_train_var = datetime.today()
        diff_train_var = end_train_var - start_train_var
        h,m,s = hours_minutes_seconds(diff_train_var)
        print(f"Train var took {h}:{m}:{s}")

    end_time = datetime.today()
    training_time = end_time - start_time
    h,m,s = hours_minutes_seconds(training_time)
    print(f"Training took {h}:{m}:{s}")
    # logger.debug(f"Done Training. Training took {h}:{m}:{s}.\n")