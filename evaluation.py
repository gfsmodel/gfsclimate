from utils.wnm_pipeline import WavenetPipeline
from utils.wnm_coor_pipeline import WavenetCoorPipeline
from utils.visualization.timeseriesgraph import Graph
from utils.preprocessing.preprocess_train_wnm import load_data_historical
import configparser
import argparse
import tensorflow as tf

parser = argparse.ArgumentParser(description='evaluation')
parser.add_argument('--year', '-y', type=int)
parser.add_argument('--month', '-m', type=int)
parser.add_argument('--day','-d', type=int)
parser.add_argument('--hour', '-hr', type=int)
parser.add_argument('--var_num', '-v', type=int)
parser.add_argument('--model', '-md', type=str, default="wnm")
args = parser.parse_args()

if __name__ == "__main__":
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        for gpu in gpus:
            try:
                tf.config.experimental.set_memory_growth(gpu, True)
            except RuntimeError as e:
                print(e)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")

    config = configparser.ConfigParser()
    config.read("remote.conf")
    c = config['wavenet']
    Pipeline = WavenetPipeline

    var_num = args.var_num
    year = args.year
    month = args.month
    day = args.day
    hour = args.hour
    model_name = args.model

    if model_name == "wnm":
        c = config['wavenet']
        Pipeline = WavenetPipeline
    elif model_name == "wnm_coor":
        c = config['wavenetCoor']
        Pipeline = WavenetCoorPipeline
    else:
        raise Exception("Undefined model name")
    # check if historical 
    # historical_file = "/usr/src/data/eval/prediction/temp_gfs_prediction_16days_var001"

    #258 gh, 261 u, 262 v, 259 t, 264 rwmr
    var_nums = [258,259,261,262,264]
    for var_num in var_nums:
        wnm = Pipeline(c, var_num)
        xdata,xdata_norm, ydata, ydata_norm, pred_output, pred_output_norm = wnm.eval(year, month, day, hour)


        # h = load_data_historical(historical_file)
        # pred_output = h['pred_output']
        # ydata = h['ydata']

        name = f"{str(year).zfill(4)}{str(month).zfill(2)}{str(day).zfill(2)}{str(hour).zfill(2)}"
        graph = Graph(config['wavenet'], var_num, name)
        graph.all_graph(ydata, pred_output)
        # for singapore
        graph.predict_actual_grid_graph(ydata=ydata, pred_output=pred_output, lat=1.5, lon=104)