from utils.preprocessing.preprocess_train_wnm import datapath_file, var_data
import configparser
import argparse
import pickle
from os.path import join

parser = argparse.ArgumentParser(description='evaluation')
parser.add_argument('--year', '-y', type=int)
parser.add_argument('--month', '-m', type=int)
parser.add_argument('--day','-d', type=int)
parser.add_argument('--hour', '-hr', type=int)
parser.add_argument('--var_num', '-v', type=int)
args = parser.parse_args()

if __name__ == "__main__":
    var_num = args.var_num
    year = args.year
    month = args.month
    day = args.day
    hour = args.hour

    config = configparser.ConfigParser()
    config.read("remote.conf")
    c = config['wavenet']

    base_path = c['base']
    csv_path = c['csv']
    model_dir = c['model_dir']
    eval_dir = c['eval_dir']

    paths = datapath_file (year,month,day,hour,base_path)
    actualvalue = var_data(paths,var_num,csv_path)

    time = f"{str(year).zfill(4)}{str(month).zfill(2)}{str(day).zfill(2)}{str(hour).zfill(2)}"
    name=f"historical {time} data var {var_num}"
    filepath = join(eval_dir, 'historical', name)

    with open(filepath, 'wb') as f:
        pickle.dump(actualvalue, f)
    