FROM tensorflow/tensorflow:2.2.0-gpu
RUN apt-get update
RUN apt-get install -y python3-grib
RUN apt-get install -y nano
RUN apt-get install -y wget
RUN pip install sklearn
RUN apt-get install -y cron
RUN pip install python-crontab
RUN pip install pandas
RUN pip install matplotlib
WORKDIR /usr/src
COPY . .
