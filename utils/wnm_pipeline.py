from utils.preprocessing.preprocess_train_wnm import normalization, split_trainval_data, load_actualvalues, denormalization, normalizing_xdata_ydata
from utils.model.wnm import Wavenet
from tensorflow import keras
from datetime import datetime, timedelta
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.layers import Flatten
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.layers import Input, Conv1D, Dense, Activation, Dropout, Lambda, Multiply, Add, Concatenate
from tensorflow.keras.optimizers import Adam
from os.path import join
import os
import pickle

class WavenetPipeline():
    def __init__(self, config, var_number):
        self.base_path = config['base']
        self.csv_path = config['csv']
        self.model_dir = config['model_dir']
        self.eval_dir = config['eval_dir']
        self.modelfilepath = join(self.model_dir, 'wnm_{}.h5'.format(str(var_number).zfill(3)))
        self.var_number = var_number
        self.model = Wavenet(config, var_number)
    
    def fit(self, year,month,day,hour, epochs):
        # next time separate preprocessing and training
        history = self.__get_full_model_saved(
            epochs=epochs,
            year=year,
            month=month,
            day=day,
            hour=hour,
            var_number=self.var_number
        )
        return history
    
    def predict(self,year,month,day,hour):
        xdata_norm,ydata_norm,maxval,minval = normalizing_xdata_ydata(
            year=year,
            month=month,
            day=day,
            hour=hour,
            var_number=self.var_number,
            base=self.base_path,
            csv_path=self.csv_path
        )
        output = self.model.predict(xdata_norm)
        return output
    
    def eval(self,year,month,day,hour):
        # year,month,day,hour for the start of evaluation
        # return xdata,xdata_norm, ydata, ydata_norm, pred_output, pred_output_norm
        xdata,ydata = load_actualvalues(
            year=year,
            month=month,
            day=day,
            hour=hour,
            var_number=self.var_number,
            base=self.base_path,
            csv_path=self.csv_path
        )
        xdata_norm,ydata_norm,maxval,minval = normalization(
            xdata=xdata,
            ydata=ydata
        )
        
        xdata_norm = xdata_norm.reshape(xdata_norm.shape[0],xdata_norm.shape[1],1)
        pred_output_norm = self.model.predict(xdata_norm)
        pred_output = denormalization(
            data_norm=pred_output_norm,
            minval=minval,
            maxval=maxval
        )
        print(pred_output.shape)
        self.__save_prediction(
            ydata=ydata,
            pred_output=pred_output,
            year=year,
            month=month,
            day=day,
            hour=hour
        )
        return xdata,xdata_norm, ydata, ydata_norm, pred_output, pred_output_norm

    def __save_prediction(self, ydata, pred_output, year, month, day, hour):
        # filename yyyymmddhh
        # create directory if does not exists
        name = f"{str(year).zfill(4)}{str(month).zfill(2)}{str(day).zfill(2)}{str(hour).zfill(2)}"
        dirpath = join(self.eval_dir, 'prediction', name)
        os.makedirs(dirpath, exist_ok=True)

        filename=join(dirpath, f'temp_gfs_prediction_16days_var{str(self.var_number).zfill(3)}')
        outfile = open(filename,'wb')
        o = {
            "ydata":ydata,
            "pred_output":pred_output
        }
        pickle.dump(o,outfile)
        outfile.close()
        print("Saving temporary prediction is successful")
        return

    # Note var_number starts from 0 to 270+
    # Timesteps is how many timestep you are taking from the actual value.
    def __get_full_model_saved(self, epochs,year,month,day,hour,var_number):
        base = self.base_path
        csv_path = self.csv_path
        model_dir = self.model_dir
        modelfilepath = self.modelfilepath
        xdata_norm,ydata_norm,maxval,minval = normalizing_xdata_ydata(year,month,day,hour,var_number,base, csv_path)
        xtrain,xval,ytrain,yval = split_trainval_data(xdata_norm,ydata_norm)
        
        history = self.model.fit(
            xtrain=xtrain,
            ytrain=ytrain,
            xval=xval,
            yval=yval,
            epochs=epochs
        )
        print('training done')
        return history

    # date is the first day of the timestep u want to predict (t=1)
    # Hour is either 0,3,6,9,12,15,18,21

    # get_full_model_saved(10,2020,5,1,0,0)