from tensorflow import keras
from datetime import datetime, timedelta
import tensorflow as tf
from utils.preprocessing.preorocess_train_wnm import normalizing_xdata_ydata, split_trainval_data, load_actualvalues, denormalization
from tensorflow.keras import layers
from tensorflow.keras.layers import Flatten
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.layers import Input, Conv1D, Dense, Activation, Dropout, Lambda, Multiply, Add, Concatenate
from tensorflow.keras.optimizers import Adam
from os.path import join
import os
class Wavenet():
    def __init__(self, config, var_number):
        self.base_path = config['base']
        self.csv_path = config['csv']
        self.model_dir = config['model_dir']
        self.modelfilepath = join(self.model_dir, 'wnm_{}.h5'.format(str(var_number).zfill(3)))
        self.var_number = var_number

        # load model if possible
        try:
            model = load_model(self.modelfilepath)
        except:
            model = self.__model_sequence()
        self.model = model

        # make new directory if does not exists
        os.makedirs(self.model_dir, exist_ok=True)        

    def fit(self, xtrain, ytrain, xval, yval, epochs):
        # training
        history = self.__model_training(self.modelfilepath,epochs,xtrain,ytrain,xval,yval)
        return history
    
    def predict(self,xdata_norm, variable=3):
        # predict
        xdata_norm = xdata_norm.reshape(xdata_norm.shape[0],xdata_norm.shape[1],variable)
        output = self.model.predict(xdata_norm)
        return output
    
    def __slice(self, x, seq_length):
        return x[:, -seq_length:,:]

    def __model_sequence(self, variable=3):
        # convolutional operation parameters
        n_filters = 128 # 32
        filter_width = 2
        dilation_rates = [2**i for i in range(8)] * 2

        # define an input history series and pass it through a stack of dilated causal convolution blocks.
        history_seq = Input(shape=(None, variable))
        x = history_seq

        skips = []
        for dilation_rate in dilation_rates:

            # preprocessing - equivalent to time-distributed dense
            x = Conv1D(16, 1, padding='same', activation='relu')(x) #value was 16

            # filter convolution
            x_f = Conv1D(filters=n_filters,
                        kernel_size=filter_width,
                        padding='causal',
                        dilation_rate=dilation_rate)(x)

            # gating convolution
            x_g = Conv1D(filters=n_filters,
                        kernel_size=filter_width,
                        padding='causal',
                        dilation_rate=dilation_rate)(x)

            # multiply filter and gating branches
            z = Multiply()([Activation('tanh')(x_f),
                            Activation('sigmoid')(x_g)])

            # postprocessing - equivalent to time-distributed dense
            z = Conv1D(16, 1, padding='same', activation='relu')(z) #value was 16

            # residual connection
            x = Add()([x, z])

            # collect skip connections
            skips.append(z)

        # add all skip connection outputs
        out = Activation('relu')(Add()(skips))

        # final time-distributed dense layers
        out = Conv1D(256, 1, padding='same')(out) #value was 128
        out = Activation('relu')(out)
        out = Dropout(.2)(out)
        out = Conv1D(1, 1, padding='same')(out)

        pred_seq_train = Lambda(self.__slice, arguments={'seq_length':128})(out) #value was 60

        model = Model(history_seq, pred_seq_train)
        return model

    def __model_training(self,modelfilepath,epochs,xtrain,ytrain,xval,yval, variable=3):
        xtrain = xtrain.reshape(xtrain.shape[0],xtrain.shape[1],variable)
        ytrain = ytrain.reshape(ytrain.shape[0],ytrain.shape[1],1)
        xval = xval.reshape(xval.shape[0],xval.shape[1],variable)
        yval = yval.reshape(yval.shape[0],yval.shape[1],1)

        model = self.model
        modelfilepath = self.modelfilepath

        batch_size = 32 #Value was 2**11
        earlyStopping = keras.callbacks.EarlyStopping(monitor='val_loss', patience=10, verbose=0, mode='min')
        mcp_save = keras.callbacks.ModelCheckpoint(modelfilepath, save_best_only=True, monitor='val_loss', mode='min')
        reduce_lr_loss = keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=5, verbose=1, epsilon=1e-4, mode='min')
        model.compile(Adam(), loss='mean_absolute_error')
        history = model.fit(xtrain, ytrain,
                                    batch_size=batch_size,
                                    epochs=epochs,
                                    validation_data = (xval,yval),
                                    callbacks=[earlyStopping,mcp_save, reduce_lr_loss])
        return history