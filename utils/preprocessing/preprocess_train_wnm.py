import pickle
import numpy as np
import pandas as pd
import pygrib
import math
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from datetime import datetime, timedelta
from os.path import join

"""# Getting datasets"""
# date is the first day of the timestep u want to predict (t=1)
def datapath_file (year,month,day,hour, base):
    t1 = datetime(year,month,day,hour)
    t_250 = t1 - timedelta(days=31,hours=6)
    t128 = t1 + timedelta(days=16)
    count = t1 - timedelta(days=31,hours=6)
    datapath = []
    timee = ['00','06','12','18']
    infile = ['003','006']
    while count < t128:
        iday = str(count)[8:10]
        imonth = str(count)[5:7]
        if str(count)[11:13] in timee:
            datapath.append(join(base,"2020{}/gfs_4_2020{}{}_{}00_{}.grb2".format(imonth,imonth,iday,str(count)[11:13].zfill(2),infile[0])))
        else:
            datapath.append(join(base, "2020{}/gfs_4_2020{}{}_{}00_{}.grb2".format(imonth,imonth,iday,str(int(str(count)[11:13])-3).zfill(2),infile[1])))
        count += timedelta(hours=3)
    return datapath

# edit the csv and add in here then it is done
def var_data(paths,var_num, csv_path):
  test = pd.read_csv(csv_path ,index_col=0)
  if var_num >= 268:
    level = 'surface'
  else:
    level = 'isobaricInhPa'
  var_shortname = test.iloc[var_num][0]
  var_layer = test.iloc[var_num][1]
  array=[]
  print(f"processing var shortname {var_shortname}")
  print(f"processing var layer {var_layer}")
  for i in paths:
    print(f"processing path {i}")
    gfs = pygrib.open(i)
    gfs_select=gfs.select(shortName=var_shortname, typeOfLevel=level)
    j=gfs_select[var_layer]
    y = j.values
    y_flatten = y.flatten('C')
    array.append(y_flatten)
  # (timestep, flatten_data)
  return array

def window_slide(data, step=8*8, input_size=250, predict_size=128):
  data_size = len(data)
  input_size = input_size
  predict_size = predict_size
  step_size=step

  xtrain = []
  ytrain = []
  for i in range(input_size, data_size-predict_size+1, step_size):
    # if the data is not enough, ignore them
    if i+predict_size > data_size:
      continue
    xtrain.append(data[i-input_size:i])
    ytrain.append(data[i:i+predict_size])
  print(len(xtrain))
  return np.array(xtrain), np.array(ytrain)


def load_actualvalues(year,month,day,hour,var_number,base,csv_path):
  paths = datapath_file (year,month,day,hour,base)
  actualvalue = var_data(paths,var_number,csv_path)

  xdata = actualvalue[:250]
  ydata = actualvalue[250:]

  #transpose xdata & ydata
  xdata = np.transpose(xdata,(1,0))
  ydata = np.transpose(ydata,(1,0))
  return xdata,ydata

def normalization(xdata, ydata):
    maxvalx = math.ceil(np.max(xdata))
    minvalx = math.floor(np.min(xdata))
    maxvaly = math.ceil(np.max(ydata))
    minvaly = math.floor(np.min(ydata))
    if maxvalx > maxvaly:
        maxval = maxvalx
    else:
        maxval = maxvaly
    if minvalx < minvaly:
        minval = minvalx
    else:
        minval = minvaly
    # catch potention zero division
    if maxval == 0 and minval == 0:
        maxval = 1
    xdata_norm = (xdata-minval)/(maxval-minval)
    ydata_norm = (ydata-minval)/(maxval-minval)

    return np.array(xdata_norm),np.array(ydata_norm), maxval,minval

def normalizing_xdata_ydata (year,month,day,hour,var_number,base,csv_path):
  xdata,ydata= load_actualvalues(year,month,day,hour,var_number,base,csv_path)
  xdata_norm, ydata_norm, maxval, minval = normalization(xdata, ydata)
  return xdata_norm,ydata_norm, maxval,minval

def denormalization(data_norm, minval, maxval):
    data = data_norm * (maxval-minval) + minval
    return data

def split_trainval_data(xdata_norm,ydata_norm):
  xtrain,xval,ytrain,yval = train_test_split(xdata_norm,ydata_norm,train_size=0.8)
  print("Train val data has been splitted")
  return xtrain,xval,ytrain,yval

def add_lat_lon(xdata):
    # xdata shape is (coor, timestep)
    # output shape is (coor, timestep, 3)
    xdata = np.array(xdata)
    timestep = xdata.shape[1]
    xdata = xdata.reshape(xdata.shape[0],xdata.shape[1],1)
    lat, lon = get_latlon()
    lat = np.tile(lat, (timestep,1)).transpose((1,0))
    lon = np.tile(lon, (timestep,1)).transpose((1,0))
    lat = lat.reshape(xdata.shape[0],xdata.shape[1],1)
    lon = lon.reshape(xdata.shape[0],xdata.shape[1],1)
    output = np.concatenate((xdata, lat, lon), axis=2)
    return output


# Making lat, lon array
def get_latlon():
  lat = []
  lon = []
  lon_count = 0
  for q in np.arange(-90,90.5,0.5):
    count = 0
    while count<720:
      lat.append(q)
      count+=1
  while lon_count < 361:
    for q in np.arange(0,360,0.5):
      lon.append(q)
    lon_count+=1
  lat.sort(reverse=True)
  return lat,lon

#lat,lon = get_latlon()

"""## Building model"""

# extract the last length time steps as the training target
def slice(x, seq_length):
    return x[:,-seq_length:,:]

def load_data_historical(data_path):
    with open(data_path, 'rb') as f:
        score = pickle.load(f)
    return score
# /usr/src/data/eval/prediction/temp_gfs_prediction_16days_var001