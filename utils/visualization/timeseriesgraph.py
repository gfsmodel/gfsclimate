import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from os.path import join
import os

class Graph():
    def __init__(self, config, var_number, filename):
        # filename = yyyymmddhh
        self.csv_path = config['csv']
        self.graph_dir = join(config['eval_dir'], "graph", filename)
        self.csv_df = pd.read_csv(self.csv_path ,index_col=0)
        self.var_number = var_number
        self.var_shortname = self.csv_df.iloc[var_number][0]
        self.filename = filename
        
        # create new if it does not exist
        if not os.path.isdir(self.graph_dir):
            os.makedirs(self.graph_dir)

    def all_graph(self, ydata, pred_output):
        # ydata is (259920, 128)
        # pred_output is (259920, 128, 1)

        self.scatter_graph(
            ydata=ydata,
            pred_output=pred_output
        )
        self.difference_grid_graph(
            ydata=ydata,
            pred_output=pred_output
        )
        self.predict_actual_grid_graph(
            ydata=ydata,
            pred_output=pred_output
        )
        self.avg_predict_actual_graph(
            ydata=ydata,
            pred_output=pred_output
        )

        self.avg_difference_graph(
            ydata=ydata,
            pred_output=pred_output
        )

    def preprocess(self, ydata, pred_output):
        ydata = np.reshape(ydata,(259920,128))
        pred_output = np.reshape(pred_output,(259920,128))
        return ydata, pred_output

    def scatter_graph(self, ydata, pred_output):
        filename = self.filename+f"_{self.var_shortname}_scatter.png"
        variable_shortname = self.var_shortname
        ydata, pred_output = self.preprocess(ydata, pred_output)

        actual = ydata
        gfs = pred_output

        actual = np.transpose(actual,(1,0))
        gfsx = np.transpose(gfs,(1,0))
        
        # Plots for given timestep (last timestep taken here) (heatmap)
        actualy = actual[-1]
        gfsx = gfsx[-1]
        gfs_diff = abs(actualy - gfsx)

        
        lat,lon = self.get_latlon()
        scatter_plot = self.fullmap_diff(gfs_diff,lat,lon,'gfs_difference at {} at timestep 128'.format(variable_shortname))
        fig = scatter_plot.get_figure()
        fig.show()
        fig.savefig(join(self.graph_dir,filename))
        return scatter_plot

    def difference_grid_graph(self, ydata, pred_output, lat=83.5, lon=140):
        filename = self.filename+f"_{self.var_shortname}_difference_grid_({lat},{lon}).png"
        variable_shortname = self.var_shortname
        ydata, pred_output = self.preprocess(ydata, pred_output)

        actual = ydata
        gfs = pred_output

        lat,lon = self.round_lat_lon(lat,lon)
        gridnum = self.get_gridnum(lat,lon)
        varnum = self.var_number

        transposed_actualy = actual[gridnum]
        gfsx = gfs[gridnum]
        gfs_diff = []
        for i in range(len(transposed_actualy)):
            gfs_diff.append(abs(gfsx[i]-transposed_actualy[i]))
        plt.figure(figsize=(8,5))
        plt.plot(gfs_diff,label='Difference')
        plt.title("Difference between predicted and actual for {} varnum {} at lat {} lon {}".format(str(variable_shortname),str(varnum), lat,lon))
        plt.ylabel("Difference away from actual")
        plt.xlabel("timestep")
        plt.legend(loc = "upper left")
        plt.show()
        plt.savefig(join(self.graph_dir,filename))
        return plt

    def predict_actual_grid_graph(self, ydata, pred_output, lat=83.5, lon=140):
        filename = self.filename+f"_{self.var_shortname}_predict_actual_grid_({lat},{lon}).png"
        variable_shortname = self.var_shortname
        ydata, pred_output = self.preprocess(ydata, pred_output)

        actual = ydata
        gfs = pred_output

        lat,lon = self.round_lat_lon(lat,lon)
        gridnum = self.get_gridnum(lat,lon)
        varnum = self.var_number
        # Plots for per latlon diff
        transposed_actualy = actual[gridnum]
        gfsx = gfs[gridnum]
        plt.figure(figsize=(8,5))
        plt.plot(gfsx,label='prediction')
        plt.plot(transposed_actualy,label='actual')
        plt.title("Predicted and actual for {} varnum {} at lat {} lon {}".format(str(variable_shortname),str(varnum),lat,lon))
        plt.ylabel("Real value")
        plt.xlabel("timestep")
        plt.legend(loc = "upper left")
        # plt.show()
        plt.savefig(join(self.graph_dir,filename))
        return 

    def avg_predict_actual_graph(self, ydata, pred_output):
        filename = self.filename+f"_{self.var_shortname}_avg_predict_actual.png"
        variable_shortname = self.var_shortname
        ydata, pred_output = self.preprocess(ydata, pred_output)

        actual = ydata
        gfs = pred_output

        # Plot avg difference -> avg_diff = abs(avg_pred - avg_act)
        transposed_actualy = actual #(gridnum,timestep)
        avg_transposed_actualy =[]
        avg_gfsx = []
        gfs_diff = []
        for i in range(128):
            avg_transposed_actualy.append(sum(transposed_actualy[:,i])/259920)
            avg_gfsx.append(sum(gfs[:,i])/259920)
        plt.figure(figsize=(8,5))
        plt.plot(avg_gfsx,label='avg_prediction')
        plt.plot(avg_transposed_actualy,label='avg_actual')
        plt.title("Avg of predicted and actual (gfs data only) for var {}".format(str(variable_shortname)))
        plt.ylabel("Real values")
        plt.xlabel("timestep")
        plt.legend(loc = "upper left")
        plt.show()
        plt.savefig(join(self.graph_dir,filename))
        return

    def avg_difference_graph(self, ydata, pred_output):
        filename = self.filename+f"_{self.var_shortname}_avg_difference.png"
        variable_shortname = self.var_shortname
        ydata, pred_output = self.preprocess(ydata, pred_output)

        actual = ydata
        gfs = pred_output

        # Plot avg difference -> avg_diff = abs(avg_pred - avg_act)
        transposed_actualy = actual #(gridnum,timestep)
        avg_transposed_actualy =[]
        avg_gfsx = []
        gfs_diff = []
        for i in range(128):
            avg_transposed_actualy.append(sum(transposed_actualy[:,i])/259920)
            avg_gfsx.append(sum(gfs[:,i])/259920)
        for i in range(128):
            gfs_diff.append(abs(avg_gfsx[i]-avg_transposed_actualy[i]))
        plt.figure(figsize=(8,5))
        plt.plot(gfs_diff,label='Difference')
        plt.title("Avg difference (gfs data only) for var {}".format(str(variable_shortname)))
        plt.ylabel("Real values")
        plt.xlabel("timestep")
        plt.legend(loc = "upper left")
        plt.show()
        plt.savefig(join(self.graph_dir,filename))
        return

    def get_latlon(self):
        lat = []
        lon = []
        lon_count = 0
        for q in np.arange(-90,90.5,0.5):
            count = 0
            while count<720:
                lat.append(q)
                count+=1
        while lon_count < 361:
            for q in np.arange(0,360,0.5):
                lon.append(q)
            lon_count+=1
        lat.sort(reverse=True)
        return lat,lon

    def myround(self, x, base=5):
        return base * round(x/base)

    def round_lat_lon(self, lat, lon):
        lat = self.myround(lat*10)
        lon = self.myround(lon*10)
        return lat/10, lon/10

    def get_dict(self, lat, lon):
        rst = {}
        for i, v in enumerate(zip(lat, lon)):
            a,b = v
            rst[(a,b)]=i
        return rst

    def get_gridnum(self, lat, lon):
        # lat (-90,90)
        # lon (-180, 180)
        lt,ln = self.get_latlon()
        rst = self.get_dict(lt,ln)
        lon = lon + 180
        return rst[(lat,lon)]


    def fullmap_diff (self, arr,lat,lon,titlee):
        listt = []

        # compensating the latlon differnces
        # lon = np.array(lon) - 180
        for i in range(259920):
            l = [lat[i],lon[i],arr[i]]
            listt.append(l)
        dictt_df = pd.DataFrame(listt,columns=['lat', 'lon', 'data'])
        scatter_plot = dictt_df.plot.scatter(x='lon',y='lat',c='data',colormap='jet',title='{}'.format(titlee))
        return scatter_plot