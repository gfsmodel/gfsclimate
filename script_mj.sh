docker rmi $(docker images -f "dangling=true" -q)
docker ps -a | grep gfs_climate_mj | awk '{print $1}' | xargs docker rm
nvidia-docker build . -t climate_mj
docker run --runtime=nvidia --name=gfs_climate_mj \
                  -v /home/clima_modelers/data:/usr/src/data \
                  -v /home/clima_modelers/model:/usr/src/model \
                  -it climate_mj

