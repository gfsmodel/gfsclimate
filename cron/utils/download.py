from datetime import datetime, timedelta
from enum import Enum
from os.path import join
import model_training_ilya
import logging
import requests
import os
import tensorflow as tf
import pytz
from pytz import timezone, utc

def customTime(*args):
    utc_dt = utc.localize(datetime.utcnow())
    my_tz = timezone("Asia/Singapore")
    converted = utc_dt.astimezone(my_tz)
    return converted.timetuple()

log_filepath = '/usr/src/data/forecast/logs/logs.txt'
logging.basicConfig( filename = log_filepath,filemode = 'a',level = logging.DEBUG, format = '%(asctime)s - %(levelname)s: %(message)s',\
                     datefmt = '%m/%d/%Y %I:%M:%S %p %Z' )
logging.Formatter.converter = customTime
logger = logging.getLogger(__name__)


class DayTime(Enum):
  MIDNIGHT = "0000"
  MORNING = "0600"
  AFTERNOON = "1200"
  EVENING = "1800"

def get_link(year, month, day, time: DayTime, fct_step):
  if not isinstance(time, DayTime):
    raise TypeError
  year = str(year).zfill(4)
  month = str(month).zfill(2)
  day = str(day).zfill(2)
  fct_step = str(fct_step).zfill(3)
  time = time.value
  return f'https://www.ncei.noaa.gov/data/global-forecast-system/access/grid-004-0.5-degree/forecast/{year}{month}/{year}{month}{day}/gfs_4_{year}{month}{day}_{time}_{fct_step}.grb2'

def get_link_day(year, month, day):
    result = []
    for time in [DayTime.MIDNIGHT, DayTime.MORNING, DayTime.AFTERNOON, DayTime.EVENING]:
        fct_3 = get_link(year, month, day, time, 3)
        fct_6 = get_link(year, month, day, time, 6)
        result.extend([fct_3, fct_6])
    return result

def download_links(links, directory):
    print('Begin file download')
    save_paths = []
    logger.debug('\nBeginning file download')
    for path in links:
        url = path
        filename = path.split('/')[-1]
        print(f"Downloading {path}")
        logger.debug(f"Downloading {path}")

        save_path = join(directory, filename)
        # check if file already exist
        if os.path.exists(save_path):
            save_paths.append(save_path)
            print(f"File already exists at {save_path}")
            logger.debug(f"File already exists at {save_path}")
            continue
        r = requests.get(url)
        os.makedirs(directory, exist_ok=True)
        if r.ok:
            with open(save_path, 'wb') as f:
                f.write(r.content)
            print(f"Successfully download {path}")
            logger.debug(f"Successfully download {path}")
            save_paths.append(save_path)
        else:
            print(f"Failed downloading {path}")
            logger.error(f"Failed downloading {path}")
    logger.debug('Done file download\n')
    return save_paths

def hours_minutes_seconds(td):
    return td.seconds//3600, (td.seconds//60)%60, td.seconds%60

if __name__ == "__main__":
    # download daily
    # 4 days before today
    days = 4
    download_day = datetime.today() - timedelta(days=days)
    year, month, day = download_day.year, download_day.month, download_day.day
    links = get_link_day(year, month, day)

    year = str(year).zfill(4)
    month = str(month).zfill(2)
    directory = f'/usr/src/data/forecast/{year}{month}/'
    save_paths = download_links(links, directory)

    # save the paths into text file i guess
    text_path = f'/usr/src/data/forecast/logs/download.txt'
    with open(text_path, 'w') as f:
        f.write("\n".join(save_paths))