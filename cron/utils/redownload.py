from datetime import datetime, timedelta
from enum import Enum
from os.path import join
import logging
import requests
import os
import tensorflow as tf
import pytz
from pytz import timezone, utc
import argparse

parser = argparse.ArgumentParser(description='running some cron jobs')
parser.add_argument('--year', type=int)
parser.add_argument('--month', type=int)
parser.add_argument('--start_day', type=int)
parser.add_argument('--stop_day', type=int)
parser.add_argument('--historical', action='store_true')
args = parser.parse_args()

class DayTime(Enum):
  MIDNIGHT = "0000"
  MORNING = "0600"
  AFTERNOON = "1200"
  EVENING = "1800"

def get_link(year, month, day, time: DayTime, fct_step):
  if not isinstance(time, DayTime):
    raise TypeError
  year = str(year).zfill(4)
  month = str(month).zfill(2)
  day = str(day).zfill(2)
  fct_step = str(fct_step).zfill(3)
  time = time.value
  return f'https://www.ncei.noaa.gov/data/global-forecast-system/access/grid-004-0.5-degree/forecast/{year}{month}/{year}{month}{day}/gfs_4_{year}{month}{day}_{time}_{fct_step}.grb2'

def get_link_historical(year, month, day, time: DayTime, fct_step):
  if not isinstance(time, DayTime):
    raise TypeError
  year = str(year).zfill(4)
  month = str(month).zfill(2)
  day = str(day).zfill(2)
  fct_step = str(fct_step).zfill(3)
  time = time.value
  return f'https://www.ncei.noaa.gov/data/global-forecast-system/access/historical/forecast/grid-004-0.5-degree/{year}{month}/{year}{month}{day}/gfs_4_{year}{month}{day}_{time}_{fct_step}.grb2'

def get_link_day(year, month, day, get_link):
    result = []
    for time in [DayTime.MIDNIGHT, DayTime.MORNING, DayTime.AFTERNOON, DayTime.EVENING]:
        fct_3 = get_link(year, month, day, time, 3)
        fct_6 = get_link(year, month, day, time, 6)
        result.extend([fct_3, fct_6])
    return result

def download_links(links, directory):
    print('Begin file download')
    save_paths = []
    for path in links:
        url = path
        filename = path.split('/')[-1]
        print(f"Downloading {path}")

        save_path = join(directory, filename)
        # check if file already exist
        if os.path.exists(save_path):
            save_paths.append(save_path)
            print(f"File already exists at {save_path}")
            continue
        r = requests.get(url)
        os.makedirs(directory, exist_ok=True)
        if r.ok:
            with open(save_path, 'wb') as f:
                f.write(r.content)
            print(f"Successfully download {path}")
            save_paths.append(save_path)
        else:
            print(f"Failed downloading {path}")
    return save_paths

def hours_minutes_seconds(td):
    return td.seconds//3600, (td.seconds//60)%60, td.seconds%60

if __name__ == "__main__":
    year, month, start_day, stop_day = args.year, args.month, args.start_day, args.stop_day
    links = []

    for day in range(start_day, stop_day+1):
        if args.historical:
            links.extend(get_link_day(year, month, day, get_link_historical))
        else:
            links.extend(get_link_day(year, month, day, get_link))

    year = str(year).zfill(4)
    month = str(month).zfill(2)
    directory = f'/usr/src/data/forecast/{year}{month}/'
    save_paths = download_links(links, directory)