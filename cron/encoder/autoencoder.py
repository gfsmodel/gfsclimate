from tensorflow.keras.layers import Conv2D, Conv3D, ConvLSTM2D, TimeDistributed, Dense, Reshape, BatchNormalization, LeakyReLU, Layer, Input, Permute, Dropout
from tensorflow.keras.models import Model, load_model, Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import CSVLogger
from tensorflow.data import Dataset
import glob
import tensorflow as tf
import numpy as np
import pygrib
import logging
import pickle
from sklearn.model_selection import train_test_split
from os.path import join
from datetime import datetime, timedelta

epochs = 100
batch_size = 1
input_param = 272
width = 720
height = 361
output_param = 1

def get_min_max(data):
  data_min = data.min(axis=(0,1),  keepdims=True)
  data_max = data.max(axis=(0,1),  keepdims=True)
  data_max[data_max == 0] = 1
  return {"max": data_max, "min": data_min}

def normalize(data, x_min, x_max):
  # change all max = 0 to 1
  norm = (data - x_min) / (x_max - x_min)
  return norm

def denormalize(norm, x_min, x_max):
  data = norm * (x_max - x_min) + x_min
  return data

def get_x(path):
  gfs = pygrib.open(path)
  parameters = ['gh', 't', 'u', 'v', 'icmr', 'rwmr', 'snmr', 'grle', 'w', "r", 'dlwrf','dswrf', 'uswrf', 'ulwrf']
  datas = np.array(list(map(lambda x: x.data()[0],gfs.select(shortName=parameters, typeOfLevel=['isobaricInhPa'])[:]))).transpose(1,2,0)
  temperature = np.array([]).reshape(361, 720, 0)
  solar = np.array([]).reshape(361, 720, 0)
  for g in gfs.select(shortName=parameters, typeOfLevel=['surface'])[:]:
    data = np.array(g.data()[0]).reshape((361,720,1))
    if g['name'] == "Temperature":
      temperature = np.concatenate([temperature, data], 2)
    else:
      solar = np.concatenate([solar, data], 2)
  datas = np.concatenate([datas, solar],2)
  return datas

def preprocess_min_max(path):
  datas = get_x(path)
  return get_min_max(datas)

def preprocess_x(path, min, max, var=None):
  x = get_x(path)
  x = normalize(x, min, max)
  # x = np.concatenate([x, day, year], 2)
  if var == None:
    return x
  else:
    return x[:,:,var:var+1]

# BASELINE
def create_model():
  kernel_size = (3,3)
  model = Sequential()
  model.add(Conv2D(128,(3,3),activation='relu', padding="same", input_shape=(height, width, input_param)))
  model.add(Conv2D(64,(3,3),activation='relu', padding="same"))
  model.add(Conv2D(32,(3,3),activation='relu', padding="same"))
  model.add(Conv2D(64,(3,3),activation='relu', padding="same"))
  model.add(Conv2D(128,(3,3),activation='relu', padding="same"))
  model.add(Dropout(0.5))
  model.add(Conv2D(input_param,(3,3),activation='linear', padding="same"))

  model.compile(optimizer="adam", loss='mse', metrics=['mse','mae'])
  return model

# later
def my_loss_fn(y_true, y_pred):
    squared_difference = tf.square(y_true - y_pred)
    return tf.reduce_mean(squared_difference, axis=-1)

# get normalization data
NORM_PATH = "./data/forecast/202004/gfs_4_20200401_0000_003.grb2"
MINMAX = preprocess_min_max(NORM_PATH)
NORM_MAX, NORM_MIN = MINMAX['max'], MINMAX['min']

def parse_file(x, var=None):
  print(f"Proccessing {x}")
  x_data = np.array([preprocess_x(path, NORM_MIN, NORM_MAX, var) for path in x])
  return x_data

def evaluation(test_data, model, var=1):
    # test data 
    real_test_data = denormalize(test_data, NORM_MIN, NORM_MAX) # (361,720, param)
    pred = model.predict(test_data[:1])
    real_pred = denormalize(pred[0], NORM_MIN, NORM_MAX)
    act_avg_mean = abs(real_pred[:,:,var]-real_test_data[:,:,var]).mean()
    norm_avg_mean = abs(pred[0][:,:,var]-test_data[0][:,:,var]).mean()
    print(act_avg_mean)
    print(norm_avg_mean)
    return 
if __name__ == "__main__":
    # BASE_URL = f"/content/drive/My Drive/Climate Modeling/Processed data/cnn/normalize/"
    BASE_URL = f"./data/forecast/202004"
    paths = glob.glob(join(BASE_URL,"*.grb2"), recursive=True)
    paths = sorted(paths)
    paths = paths[:32]
    print(len(paths))

    seed = 1000
    train_data_path, test_data_path = train_test_split(paths, test_size=0.2, random_state=seed)
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        for gpu in gpus:
            try:
                tf.config.experimental.set_memory_growth(gpu, True)
            except RuntimeError as e:
                print(e)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")

    train_data = parse_file(train_data_path)
    test_data = parse_file(test_data_path)

    save_path = f"autoencoder.h5"
    autoencoder = create_model()
    print(autoencoder.summary())
    mc = tf.keras.callbacks.ModelCheckpoint(save_path, monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=False)
    autoencoder.fit(x=train_data, y=train_data, epochs=epochs, validation_data=(test_data, test_data), callbacks=[mc], batch_size=batch_size)
    autoencoder.save(save_path)

    # 25/25 [==============================] - 10s 391ms/step - loss: 0.0094 - mse: 0.0094 - mae: 0.0531 - val_loss: 0.0094 - val_mse: 0.0094 - val_mae: 0.0530
    # evaluation
    print("Evaluating")
    var = 1
    real_test_data = denormalize(test_data[0], NORM_MIN, NORM_MAX)
    pred = autoencoder.predict(test_data[:1])
    real_pred = denormalize(pred[0], NORM_MIN, NORM_MAX)
    act_avg_mean = abs(real_pred[:,:,var]-real_test_data[:,:,var]).mean()
    norm_avg_mean = abs(pred[0][:,:,var]-test_data[0][:,:,var]).mean()
    print(act_avg_mean)
    print(norm_avg_mean)
