from crontab import CronTab
import argparse

parser = argparse.ArgumentParser(description='running some cron jobs')
exclusive = parser.add_mutually_exclusive_group()
exclusive.add_argument('--start', action='store_true', help='start job')
exclusive.add_argument('--stop', action='store_true', help='stop job')
args = parser.parse_args()
if __name__ == "__main__":
    my_cron = CronTab(user='root')
    if args.start:
        job = my_cron.new(command='/usr/bin/python3.6 /usr/src/climate/cron/utils/download.py', comment='download')
        job.every(1).day()
        my_cron.write()
        print("Job started")

        job = my_cron.new(command='/usr/bin/python3.6 /usr/src/climate/cron/train.py --start 0 --stop 70 --name 1', comment='model_1')
        job.every(1).day()
        my_cron.write()
        print("Job started")

        job = my_cron.new(command='/usr/bin/python3.6 /usr/src/climate/cron/train.py --start 70 --stop 140 --name 2', comment='model_2')
        job.every(1).day()
        my_cron.write()
        print("Job started")       

        job = my_cron.new(command='/usr/bin/python3.6 /usr/src/climate/cron/train.py --start 140 --stop 210 --name 3', comment='model_3')
        job.every(1).day()
        my_cron.write()
        print("Job started")    

        job = my_cron.new(command='/usr/bin/python3.6 /usr/src/climate/cron/train.py --start 210 --stop 275 --name 4', comment='model_4')
        job.every(1).day()
        my_cron.write()
        print("Job started")  
    else:
        for job in my_cron:
            my_cron.remove(job)
            my_cron.write()
            print("Job removed")
