from datetime import datetime, timedelta
from enum import Enum
from os.path import join
import wavenet_training
import logging
import requests
import os
import tensorflow as tf
import pytz
from pytz import timezone, utc

import argparse

parser = argparse.ArgumentParser(description='running some cron jobs')
parser.add_argument('--start', default=0, type=int, help="The start of the model")
parser.add_argument('--stop', default=275, type=int, help="The end of the model")
parser.add_argument('--name', type=str)
args = parser.parse_args()

log_filepath = f'/usr/src/data/forecast/logs/wavenet/logs_model_{args.name}.txt'

def customTime(*args):
    utc_dt = utc.localize(datetime.utcnow())
    my_tz = timezone("Asia/Singapore")
    converted = utc_dt.astimezone(my_tz)
    return converted.timetuple()

logging.basicConfig( filename = log_filepath, filemode = 'a',level = logging.DEBUG, format = '%(asctime)s - %(levelname)s: %(message)s',\
                     datefmt = '%m/%d/%Y %I:%M:%S %p %Z' )
logging.Formatter.converter = customTime
logger = logging.getLogger(__name__)

def get_time():
    delay_days = 4
    download_day = datetime.today() - timedelta(days=delay_days)
    hour = 0
    return download_day.year, download_day.month, download_day.day, hour

def hours_minutes_seconds(td):
    return td.seconds//3600, (td.seconds//60)%60, td.seconds%60

if __name__ == "__main__":
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        for gpu in gpus:
            try:
                tf.config.experimental.set_memory_growth(gpu, True)
            except RuntimeError as e:
                print(e)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")

    base = "/usr/src/data/forecast"
    csv_path='/usr/src/model/file name to variable shortname and layers.csv'
    model_dir="/usr/src/model/wnm"
    epoch = 10
    # year, month, day, hour = get_time()
    year, month, day, hour = 2020, 10, 1, 0

    # train
    logger.debug("Start Training")
    # time the training
    start_time = datetime.today()
    for i in range(args.start, args.stop):
        start_train_var = datetime.today()

        wavenet_training.get_full_model_saved(epoch, year, month, day, hour, i, base, csv_path, model_dir)

        logger.debug(f"Successfully trained on var {str(i).zfill(3)}")
        print(f"Successfully trained on var {str(i).zfill(3)}")
        end_train_var = datetime.today()
        diff_train_var = end_train_var - start_train_var
        h,m,s = hours_minutes_seconds(diff_train_var)
        print(f"Train var took {h}:{m}:{s}")

    end_time = datetime.today()
    training_time = end_time - start_time
    h,m,s = hours_minutes_seconds(training_time)
    print(f"Training took {h}:{m}:{s}")
    logger.debug(f"Done Training. Training took {h}:{m}:{s}.\n")