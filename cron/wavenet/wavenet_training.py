# -*- coding: utf-8 -*-

# Import

import pickle
import numpy as np
import pandas as pd
import pygrib
import math
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from tensorflow import keras
from datetime import datetime, timedelta
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.layers import Flatten
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.layers import Input, Conv1D, Dense, Activation, Dropout, Lambda, Multiply, Add, Concatenate
from tensorflow.keras.optimizers import Adam
from os.path import join

"""# Getting datasets"""

# saving data
# date is the first day of the timestep u want to predict (t=1)
def datapath_file (year,month,day,hour, base):
    t1 = datetime(year,month,day,hour)
    t_250 = t1 - timedelta(days=31,hours=6)
    t128 = t1 + timedelta(days=16)
    count = t1 - timedelta(days=31,hours=6)
    datapath = []
    timee = ['00','06','12','18']
    infile = ['003','006']
    while count < t128:
        iday = str(count)[8:10]
        imonth = str(count)[5:7]
        if str(count)[11:13] in timee:
            datapath.append(join(base,"2020{}/gfs_4_2020{}{}_{}00_{}.grb2".format(imonth,imonth,iday,str(count)[11:13].zfill(2),infile[0])))
        else:
            datapath.append(join(base, "2020{}/gfs_4_2020{}{}_{}00_{}.grb2".format(imonth,imonth,iday,str(int(str(count)[11:13])-3).zfill(2),infile[1])))
        count += timedelta(hours=3)
    return datapath

# output of 250+128 data of given variable number 

# edit the csv and add in here then it is done
def var_data(paths,var_num, csv_path):
  test = pd.read_csv(csv_path ,index_col=0)
  if var_num >= 268:
    level = 'surface'
  else:
    level = 'isobaricInhPa'
  var_shortname = test.iloc[var_num][0]
  var_layer = test.iloc[var_num][1]
  array=[]
  for i in paths:
    print(f"processing path {i}")
    gfs = pygrib.open(i)
    gfs_select=gfs.select(shortName=var_shortname, typeOfLevel=level)
    j=gfs_select[var_layer]
    y = j.values 
    y_flatten = y.flatten('C')
    array.append(y_flatten)
  return array

#loading xdata ydata

def load_actualvalues(year,month,day,hour,var_number,base,csv_path):
  paths = datapath_file (year,month,day,hour,base)
  actualvalue = var_data(paths,var_number,csv_path)

  xdata = actualvalue[:250]
  ydata = actualvalue[250:]

  #transpose xdata & ydata
  xdata = np.transpose(xdata,(1,0))
  ydata = np.transpose(ydata,(1,0))
  return xdata,ydata


def normalizing_xdata_ydata (year,month,day,hour,var_number,base,csv_path):
  xdata,ydata= load_actualvalues(year,month,day,hour,var_number,base,csv_path)
  maxvalx = math.ceil(np.max(xdata))
  minvalx = math.floor(np.min(xdata))
  maxvaly = math.ceil(np.max(ydata))
  minvaly = math.floor(np.min(ydata))
  if maxvalx > maxvaly:
    maxval = maxvalx
  else:
    maxval = maxvaly
  if minvalx < minvaly:
    minval = minvalx
  else:
    minval = minvaly
  # catch potention zero division
  if maxval == 0 and minval == 0:
      maxval = 1
  xdata_norm = (xdata-minval)/(maxval-minval)
  ydata_norm = (ydata-minval)/(maxval-minval)
  return np.array(xdata_norm),np.array(ydata_norm), maxval,minval

def split_trainval_data(xdata_norm,ydata_norm):
  xtrain,xval,ytrain,yval = train_test_split(xdata_norm,ydata_norm,train_size=0.8)
  print("Train val data has been splitted")
  return xtrain,xval,ytrain,yval

# Making lat, lon array
def get_latlon():
  lat = []
  lon = []
  lon_count = 0
  for q in np.arange(-90,90.5,0.5):
    count = 0
    while count<720:
      lat.append(q)
      count+=1
  while lon_count < 361:
    for q in np.arange(0,360,0.5):
      lon.append(q)
    lon_count+=1
  lat.sort(reverse=True)
  return lat,lon

#lat,lon = get_latlon()

"""## Building model"""

# extract the last length time steps as the training target
def slice(x, seq_length):
    return x[:,-seq_length:,:]

def model_sequence():
  # convolutional operation parameters
  n_filters = 128 # 32 
  filter_width = 2
  dilation_rates = [2**i for i in range(8)] * 2 

  # define an input history series and pass it through a stack of dilated causal convolution blocks. 
  history_seq = Input(shape=(None, 1))
  x = history_seq

  skips = []
  for dilation_rate in dilation_rates:
      
      # preprocessing - equivalent to time-distributed dense
      x = Conv1D(16, 1, padding='same', activation='relu')(x) #value was 16
      
      # filter convolution
      x_f = Conv1D(filters=n_filters,
                  kernel_size=filter_width, 
                  padding='causal',
                  dilation_rate=dilation_rate)(x)
      
      # gating convolution
      x_g = Conv1D(filters=n_filters,
                  kernel_size=filter_width, 
                  padding='causal',
                  dilation_rate=dilation_rate)(x)
      
      # multiply filter and gating branches
      z = Multiply()([Activation('tanh')(x_f),
                      Activation('sigmoid')(x_g)])
      
      # postprocessing - equivalent to time-distributed dense
      z = Conv1D(16, 1, padding='same', activation='relu')(z) #value was 16
      
      # residual connection
      x = Add()([x, z])    
      
      # collect skip connections
      skips.append(z)

  # add all skip connection outputs 
  out = Activation('relu')(Add()(skips))

  # final time-distributed dense layers 
  out = Conv1D(256, 1, padding='same')(out) #value was 128
  out = Activation('relu')(out)
  out = Dropout(.2)(out)
  out = Conv1D(1, 1, padding='same')(out)

  pred_seq_train = Lambda(slice, arguments={'seq_length':128})(out) #value was 60

  model = Model(history_seq, pred_seq_train)
  return model

def model_training(modelfilepath,epochs,xtrain,ytrain,xval,yval):
  xtrain = xtrain.reshape(xtrain.shape[0],xtrain.shape[1],1)
  ytrain = ytrain.reshape(ytrain.shape[0],ytrain.shape[1],1)
  xval = xval.reshape(xval.shape[0],xval.shape[1],1)
  yval = yval.reshape(yval.shape[0],yval.shape[1],1)

  # load model if possible
  try:
    model = load_model(modelfilepath)
  except:
    model = model_sequence()
  batch_size = 32 #Value was 2**11
  earlyStopping = keras.callbacks.EarlyStopping(monitor='val_loss', patience=10, verbose=0, mode='min')
  mcp_save = keras.callbacks.ModelCheckpoint(modelfilepath, save_best_only=True, monitor='val_loss', mode='min')
  reduce_lr_loss = keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=5, verbose=1, epsilon=1e-4, mode='min')
  model.compile(Adam(), loss='mean_absolute_error')
  history = model.fit(xtrain, ytrain,
                              batch_size=batch_size,
                              epochs=epochs,
                              validation_data = (xval,yval),
                              callbacks=[earlyStopping,mcp_save, reduce_lr_loss])

def predicted_output(wnm_model,xdata_norm):
  xdata_norm = xdata_norm.reshape(xdata_norm.shape[0],xdata_norm.shape[1],1)
  output = wnm_model.predict(xdata_norm)
  return output

# Note var_number starts from 0 to 270+
# Timesteps is how many timestep you are taking from the actual value.

def get_full_model_saved(epochs,year,month,day,hour,var_number, 
    base = "/content/drive/My Drive/Climate Modeling",
    csv_path='/content/drive/My Drive/Climate Modeling/file name to variable shortname and layers.csv',
    model_dir="/content/drive/My Drive/Intern 2020/GFS12"):
  xdata_norm,ydata_norm,maxval,minval = normalizing_xdata_ydata (year,month,day,hour,var_number,base, csv_path)
  xtrain,xval,ytrain,yval = split_trainval_data(xdata_norm,ydata_norm)
  modelfilepath = join(model_dir, 'wnm_{}.h5'.format(str(var_number).zfill(3)))
  model_training(modelfilepath,epochs,xtrain,ytrain,xval,yval)
  print('training done')

# date is the first day of the timestep u want to predict (t=1)
# Hour is either 0,3,6,9,12,15,18,21

# get_full_model_saved(10,2020,5,1,0,0)
