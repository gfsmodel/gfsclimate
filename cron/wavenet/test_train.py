from datetime import datetime, timedelta
from enum import Enum
from os.path import join
import wavenet_training
import logging
import requests
import os
import tensorflow as tf
import pytz
from pytz import timezone, utc

import argparse
def hours_minutes_seconds(td):
    return td.seconds//3600, (td.seconds//60)%60, td.seconds%60

def get_time():
    delay_days = 4
    download_day = datetime.today() - timedelta(days=delay_days)
    hour = 0
    return download_day.year, download_day.month, download_day.day, hour

if __name__ == "__main__":
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        for gpu in gpus:
            try:
                tf.config.experimental.set_memory_growth(gpu, True)
            except RuntimeError as e:
                print(e)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    
    # base = data_dir
    base = "/usr/src/data/forecast"
    csv_path='/usr/src/model/file name to variable shortname and layers.csv'
    model_dir="/usr/src/model/wnm"

    epoch = 10
    # year, month, day, hour = get_time()
    year, month, day, hour = 2020, 10, 1, 0
    # time the training
    start_time = datetime.today()
    wavenet_training.get_full_model_saved(epoch, year, month, day, hour, 0, base, csv_path, model_dir)

    end_time = datetime.today()
    training_time = end_time - start_time
    h,m,s = hours_minutes_seconds(training_time)
    print(f"Training took {h}:{m}:{s}")